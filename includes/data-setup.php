<?php
	    // Random venue list

        $venueNames = array(
            0 => "Alex's Bar",             
            1 => "Auld Dubliner Irish Pub",            
            2 => "Belmont Station",             
            3 => "Blue Dog Tavern ",            
            4 => "Boomer's Cocktail Lounge",            
            5 => "Borders",             
            6 => "Cirivello's",             
            7 => "Club Ripples",           
            8 => "Di Piazza's",             
            9 => "El Dorado Restaurant",            
           10 => "Fern's Cocktails",            
           11 => "Frisco's Carhop Bar & Grill",             
           12 => "Gladstone's",             
           13 => "Khoury's",            
           14 => "Liquid Lounge", 
           15 => "Lush Lounge",             
           16 => "MaiTai Bar at The Pike",              
           17 => "Max Steiners",            
           18 => "PCH Club",            
           19 => "Pete's at the Beach",             
           20 => "Que Sera",            
           21 => "Queen Mary",          
           22 => "Rhythm Lounge",           
           23 => "SeaPort Marina Hotel",            
           24 => "Shore Ultra Lounge"          
        );

        // DNA option arrays

        $musicOpts = array(
           0 => "Rap",
           1 => "Indie Rock",
           2 => "Techno",
           3 => "R&B",
           4 => "Latin",
           5 => "Rock",
           6 => "Jazz",
           7 => "Pop",
           8 => "Reggae"
        );

        $atmosphereOpts = array(
           0 => "VIP",
           1 => "Cocktail Lounge",
           2 => "Sports Bar",
           3 => "Hipster Lounge",
           4 => "Dance Club",
           5 => "Concert Hall"
        );

        $spendingOpts = array(
           0 => "$",
           1 => "$$",
           2 => "$$$",
           3 => "$$$$"
        );

?>