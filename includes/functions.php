<?php


function getSetup() {
    $_SESSION['setup'] = array();

    $data = array();
    $getMusicList = new httpGet('data', 'musicList', $data);
    $musicList = json_decode(httpRequest::makeGetRequest($getMusicList));

    $musicObjArray = $musicList->body;

    $music = array();
    foreach ($musicObjArray as $single) {
        $music[$single->id] = ucwords($single->name);
    }

    $_SESSION['setup']['music'] = $music; 

    $data = array();
    $getAtmosphereList = new httpGet('data', 'atmosphereList', $data);
    $atmosphereList = json_decode(httpRequest::makeGetRequest($getAtmosphereList));

    $atmosphereObjArray = $atmosphereList->body;

    $atmosphere = array();
    foreach ($atmosphereObjArray as $single) {
        $atmosphere[$single->id] = ucwords($single->name);
    }

    $_SESSION['setup']['atmosphere'] = $atmosphere; 

    $_SESSION['setup']['spending'] = array(1 => '$', '$$', '$$$', '$$$$');

    $data = array();
    $getCitiesList = new httpGet('data', 'cities', $data);
    $citiesList = json_decode(httpRequest::makeGetRequest($getCitiesList));

    $citiesObjArray = $citiesList->body;

    $cities = array();
    foreach ($citiesObjArray as $single) {
        $cities[$single->id] = ucwords(strtolower($single->name));
    }

    $_SESSION['setup']['cities'] = $cities;
    $_SESSION['home'] = 'http://ignight.dev';

}

function getTrendingList($user) {

    $venues = array();

    $data = array('userId' => $user->getId(), 'cityId' => $user->getCityId());
    $getVenues = new httpGet('user', 'trendingList', $data);
    $venueInitialResponse = json_decode(httpRequest::makeGetRequest($getVenues));

    if ($venueInitialResponse && $venueInitialResponse->res == 1) {

        foreach($venueInitialResponse->body as $venue) {

            $data = array('venueId' => $venue->venueId, 'cityId' => $user->getCityId(), 'userId' => $user->getId());
            $getVenue = new httpGet('venue', 'getVenueDataForUser', $data);
            $venueDetailResponse = json_decode(httpRequest::makeGetRequest($getVenue));

            if ($venueDetailResponse && $venueDetailResponse->res == 1) {

                 $venues[] = Venue::initFromJson($venueDetailResponse->body);

            }
        
        }

    }
    return $venues;
}

function getVenueDetails($user, $venueId) {
    $data = array('venueId' => $_GET['id'], 'cityId' => $user->getCityId(), 'userId' => $user->getId());
    $getVenue = new httpGet('venue', 'getVenueDataForUser', $data);
    $response = json_decode(httpRequest::makeGetRequest($getVenue));

    if ($response && $response->res == 1) {
         $venue = Venue::initFromJson($response->body);
    }

    $data = array('venueId' => $venue->getId(), 'cityId' => $user->getCityId());
    $getVenue = new httpGet('venue', 'getCrowdStats', $data);
    $response = json_decode(httpRequest::makeGetRequest($getVenue));

    if ($response && $response->res == 1) {

        $dna = $response->body;

        $venueDna = array();
        $venueDna['music'] = $dna->musicTypeMode;
        $venueDna['atmosphere'] = $dna->atmosphereTypeMode;
        $venueDna['age'] = $dna->ageBucketMode;
        $venueDna['spending'] = $dna->avgSpendingLimit;

        $venue->setVenueDna($venueDna);
  
    }

    return $venue;

}

function getDnaForUsers($userOneId, $userTwoId) {
    $data = array('userOneId' => $userOneId, 'userTwoId' => $userTwoId);
    $getDna = new httpGet('user', 'userToUserDna', $data);
    $response = json_decode(httpRequest::makeGetRequest($getDna));

    return $response->body->userToUserDna;
}

function getDnaClass($dna) {
    if ($dna > 0.80) {
        $dnaClass = 'high';
    } else if ($dna > 0.60) {
        $dnaClass = 'med-high';
    } else if ($dna > 0.40) {
        $dnaClass = 'med';
    } else if ($dna > 0.10) {
        $dnaClass = 'low';
    } else {
        $dnaClass = '';
    }

    return $dnaClass;
}





