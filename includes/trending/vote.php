<?php
	
	if (isset($_COOKIE["PHPSESSID"]) && !isset($_SESSION)) {

		require_once('../../classes/httpRequest.php');
		require_once('../../classes/user.php');
		require_once('../../classes/dna.php');

        session_start();
    }

	if (isset($_GET['vote']) && $_GET['vote'] == 'true') {
       
		

		$user = $_SESSION['user'];

		$vote = new httpPost('venue', array('type' => $_GET['type'], 'userId' => $user->getId(), 'venueId' => $_GET['id']));
		$response = httpRequest::makePostRequest($vote);
		
		$data = array('userId' => $user->getId());
		$getInfo = new httpGet('user', 'getInfo', $data);
		$response = json_decode(httpRequest::makeGetRequest($getInfo));

		$_SESSION['user'] = User::initFromJson($response->body);
		$user = $_SESSION['user'];

		$venueVotes = $user->getVenueVotes();
		$id = $_GET['id'];
	}
?>


<div class="btn-group">
    <a data-id="<?php echo $id; ?>" class="btn doVote upVote <?php if (in_array($id,$venueVotes['upVotes'])) { echo 'btn-success active'; }  ?>"><i class="icon-arrow-up"></i></a>
	<a data-id="<?php echo $id; ?>" class="btn doVote downVote <?php if (in_array($id,$venueVotes['downVotes'])) { echo 'btn-info active'; }  ?>"><i class="icon-arrow-down"></i></a>
</div>