<?php

	// Load venue

	if (isset($_GET['id'])) {
		$venue = getVenueDetails($user, $_GET['id']);
	}

	// echo '<pre>';
	// print_r($venue);
	// echo '</pre>';
			
	// Parse venue details

	$id = $venue->getId();
 	$venueName = $venue->getName();
 	$userValue = $venue->getUserValue();

 	$address = $venue->getAddress();
 	$city = ucwords(strtolower($venue->getCity()));
 	$state = $venue->getState();
 	$zip = $venue->getZip();
 	$phone = $venue->getPhone();
 	$url = $venue->getUrl();
 	$venueDna = $venue->getVenueDna();
 	$userValueClass = $venue->getUserValueClass($userValue);
 	$venueVotes = $user->getVenueVotes();

 	// Parse venueDna

 	$music = $_SESSION['setup']['music'][$venueDna['music']];
 	$atmosphere = $_SESSION['setup']['atmosphere'][$venueDna['atmosphere']];
 	$age = $venueDna['age'];
 	$spending = $_SESSION['setup']['spending'][$venueDna['spending']];

?>

<div class="map">
	<iframe width="300" height="100" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo str_replace(' ', '+', $address . '+' . $zip); ?>&amp;aq=0<?php echo str_replace(' ', '+', $street . '+' . $zip); ?>&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo str_replace(' ', '+', $street . '+' . $zip); ?>&amp;output=embed"></iframe>
	<span class="rating <?php echo $userValueClass; ?>"><?php echo floor($userValue); ?>%</span>
</div>
<hr />

<div class="venue-card clearfix">
	<div class="vote">
        <?php include('vote.php'); ?>
    </div>
    <div class="venue-info">
	    <span class="venue-name"><?php echo $venueName; ?></span>
	    <span class="venue-distance"><?php echo $address; ?></span>
	    <span class="venue-distance"><?php echo $city . ', ' . $state . ' ' . $zip; ?></span>
	</div>
	<div class="contact">
        <div class="btn-group">
            <a href="tel:1-<?php echo $phone; ?>" class="btn tt" data-toggle="tooltip" title="<?php echo $phone; ?>"><i class="icon-info-sign"></i></a>
            <a href="http://<?php echo $url; ?>" class="btn tt" data-toggle="tooltip" title="<?php echo $url; ?>"><i class="icon-share-alt"></i></a>
        </div>
    </div>
</div>

<hr />

<div class="subtitle trending-crowd clearfix">
	<h5><i class="icon icon-user"></i>Trending Crowd</h5>
	<ul>
		<li class="tt" data-toggle="tooltip" title="Music - <?php echo $music; ?>"><?php echo $music; ?></li>
		<li class="tt" data-toggle="tooltip" title="Atmosphere - <?php echo $atmosphere; ?>"><?php echo $atmosphere; ?></i></li>
		<li class="tt" data-toggle="tooltip" title="Age - <?php echo $age; ?>"><?php echo $age; ?></li>
		<li class="tt" data-toggle="tooltip" title="Spending - <?php echo $spending; ?>"><?php echo $spending; ?></li>
	</ul>
</div>

<hr />

<div class="subtitle venue-buzz clearfix">
	<h5><i class="icon icon-comment"></i>Venue Buzz</h5>
	
	<?php 
		$limit = 5;
		include('includes/buzz/comment-list.php'); 
	?>

	<a class="btn btn-success full" href="buzz.php?id=<?php echo $id; ?>">Join the <strong>Conversation</strong></a>
	
</div>
