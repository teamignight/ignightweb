 <?php 

 	function compareUserValue($a, $b)
	{
	  return $b->userValue - $a->userValue;
	}

	usort($venues, 'compareUserValue');

 	foreach ($venues as $venue) { 

	 	// Parse variables

 		$id = $venue->getId();
	 	$venueName = $venue->getName();
	 	$userValue = $venue->getUserValue();
	 		
	 	$userValueClass = $venue->getUserValueClass($userValue);

	 	$venueVotes = $user->getVenueVotes();


 ?>

		 <li>
		 	<a href="venue.php?id=<?php echo $id; ?>">
			    <div class="clearfix">
			        <span class="rating <?php echo $userValueClass; ?>"><?php echo floor($userValue); ?>%</span>
			        <div class="venue-info">
			            <span class="venue-name"><?php echo $venueName; ?></span>
			            <span class="venue-distance"><?php echo '12.5'; ?> Miles</span>
			        </div>
			        <div class="vote">
			            <?php include('vote.php'); ?>
			        </div>
			    </div>
			</a>
		</li>

<?php } ?>