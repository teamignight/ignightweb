<?php
    
    require_once('classes/dna.php');
    require_once('classes/venue.php');
    require_once('classes/user.php');
    require_once('classes/comment.php');
    require_once('classes/httpRequest.php');
    require_once('includes/functions.php');

    if (isset($_COOKIE["PHPSESSID"])) {
        session_start();
    }

    if (isset($_GET['s']) && $_GET['s'] = 'kill') {
        session_unset();
        session_destroy();
    }

    if (!isset($_SESSION['setup'])) {
        getSetup();
    }

    if (!isset($_SESSION['user']) && strstr($_SERVER['REQUEST_URI'], 'login.php') == false) {
        if (!strstr($_SERVER['REQUEST_URI'], 'register.php')) {
            header('Location:login.php');
        }
    } else {
        if (isset($_SESSION['user'])) {

            // debug the user/getInfo call ***TEMPORARY***
            if (isset($_SESSION['user']->userID)) {
                $_SESSION['user']->userId = $_SESSION['user']->userID;
                unset($_SESSION['user']->userID);
            }

            $user = $_SESSION['user'];
        }
    }
    

    // $venueCount = 25;
    // $userCount = 100;
    // $commentsCount = 500;

    // Randomly generate a snapshot of user and venue activity (use ?s=kill to reset the session)
    // include('includes/data-setup.php');
    // include('functions/rand-venue-generator.php');
    // include('functions/rand-user-generator.php');
    // include('functions/rand-comment-generator.php');

    // $currentUser = $_SESSION['users'][0];
    // $users = $_SESSION['users'];
    // $venues = $_SESSION['venues'];
    // $comments = $_SESSION['comments'];

?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>IgNight - Ignite your night!</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>

    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->

    <!-- Add your site or application content here -->