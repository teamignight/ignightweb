<?php 

    $page = 'login';
    $pageTitle = 'IgNight Sign Up';
    $section = 'setup';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    if (isset($_SESSION['error'])) {
    	$error = $_SESSION['error'];
    }

?>

<div id="main">
	
	<form class="login-form" method="post" action="functions/registration-handler.php">

		<?php 

			if (isset($error)) {
				echo '<p class="error">' . $error . '</p>';
			}
			unset($_SESSION['error']);

		?>
	
		<input type="hidden" name="type" value="add" />

		<p class="centered">Sign up with</p>
		<p class="centered sm-btns">
			<a class="btn btn-primary">Facebook</a>
			<a class="btn btn-info">Twitter</a>
		</p>
		<p class="centered">or</p>
		<hr />
		<label>Email</label>
		<input name="userName" type="text" value="" />
		<label>Password</label>
		<input name="password" type="password" value="" />
		<label>Confirm Password</label>
		<input name="confirmpassword" type="password" value="" />
		
		<input class="btn btn-success full" type="submit" value="Sign Up" />

	</form>

</div>

<?php include('includes/footer.php'); ?>
