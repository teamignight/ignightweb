<?php

	$topMusic = array();

	$y = 0;
	while ($y <= 5) {
		foreach ($musicOpts as $music) {
			if (rand(0,5) == 5) {
				if (!in_array($music, $topMusic)) {
					$topMusic[] = $music;
					$y ++;
					if ($y == 5) {
						break 2;
					}
				}
			}
		}
	}


	$topAtmospheres = array();

	$y = 0;
	while ($y <= 3) {
		foreach ($atmosphereOpts as $atmosphere) {
			if (rand(0,5) == 5) {
				if (!in_array($atmosphere, $topAtmospheres)) {
					$topAtmospheres[] = $atmosphere;
					$y ++;
					if ($y == 3) {
						break 2;
					}
				}
			}
		}
	}
	
	$topVenues = array();

	$y = 0;
	while ($y <= 3) {
		foreach ($_SESSION['venues'] as $venue) {
			if (rand(0,5) == 5) {
				if (!in_array($venue->getId(), $topVenues)) {
					$topVenues[] = $venue->getId();
					$y ++;
					if ($y == 3) {
						break 2;
					}
				}
			}
		}
	}

	$spending = $spendingOpts[rand(0,3)];

	$dna = new Dna;
	$dna->setTopMusic($topMusic);
	$dna->setTopAtmospheres($topAtmospheres);
	$dna->setTopVenues($topVenues);
	$dna->setAge(rand(17,39));
	$dna->setSpending($spending);


?>