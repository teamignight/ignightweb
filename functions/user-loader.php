<?php

	require_once('../classes/httpRequest.php');

	$file = "csv/users.csv";

	$f = fopen($file, "r");

	$holder = array();

	$i = 0;
	$x = -1;
	while ($record = fgetcsv($f)) {

		foreach($record as $field) {

			if ($i % 19 == 0) {
				$x++;
			}

			$field = str_replace('. ', '', trim(preg_replace('/\s+/', ' ', $field)));
        	$holder[$x][] = $field;
        	$i ++;
    	}
	}

	$trash = array_pop($holder);

	$x = 0;
	foreach ($holder as $user) {

		// begin att post request data setup
		$dataAdd['type'] = 'add';
		$dataAdd['userName'] = $user[5] . '@gmail.com';
		$dataAdd['password'] = $user[6];

		// make the add request
		$postAdd = new httpPost('user', $dataAdd);
		$responseAdd = httpRequest::makePostRequest($postAdd);

		// begin setUserInfo post request data setup
		$dataSetInfo['type'] = 'setUserInfo';
		$dataSetInfo['userId'] = $responseAdd->body->id;
		$dataSetInfo['firstName'] = $user[0];
		$dataSetInfo['lastName'] = $user[1];
		$dataSetInfo['cityId'] = (int) $user[2];
		$dataSetInfo['genderId'] = (int) $user[3];
		$dataSetInfo['age'] = (int) $user[4];
		$dataSetInfo['picture'] = 'default.jpg';

		// make the setUserInfo request
		$postAdd = new httpPost('user', $dataSetInfo);
		$responseSetInfo = httpRequest::makePostRequest($postAdd);

		// begin setDna post request data setup
		$dataSetDna['type'] = 'setDna';
		$dataSetDna['userId'] = $responseAdd->body->id;
		$dataSetDna['music'] = array((int) $user[7], (int) $user[8], (int) $user[9], (int) $user[10], (int) $user[11]);
		$dataSetDna['atmosphere'] = array((int) $user[12], (int) $user[13], (int) $user[14]);
		$dataSetDna['venue'] = array((int) $user[15], (int) $user[16], (int) $user[17]);
		$sl = $user[18] - 1;
		$dataSetDna['spendingLimit'] = (int) $sl;

		// make the setDNA request
		$postSetDna = new httpPost('user', $dataSetDna);
		$responseSetDna = httpRequest::makePostRequest($postSetDna);

		$x ++;
		// Number to check before exiting script
		if ($x == 25) {
			break;
		}

		// delay the next request by x seconds
		// sleep(30);

		

	}

	fclose($f);
