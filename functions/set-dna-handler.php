<?php  

	if (isset($_COOKIE["PHPSESSID"])) {
        session_start();
    }

    $home = $_SESSION['home'];

	require_once('../classes/httpRequest.php');

	if ($_GET['type'] == 'music') {
    	$type = 'updateMusic';
    	$call = 'music';
    } else if ($_GET['type'] == 'atmosphere') {
    	$type = 'updateAtmospheres';
    	$call = 'atmosphere';
    } else if ($_GET['type'] == 'spending') {
    	$type = 'updateSpendingLimit';
    	$call = 'spendingLimit';
    } else if ($_GET['type'] == 'info') {
    	$type = 'setUserInfo';
    	$call = 'info';
    } else if ($_GET['type'] == 'venues') {
    	$type = 'updateVenues';
    	$call = 'venue';
    }

    if ($call == 'info') {

    	$data = array('type' => $type);
    	$data += $_POST;
    	$data['userId'] = $_SESSION['user']->userId;

    	postToServer($data, $call);

    } else if ($call == 'spendingLimit') {
    	
    	$data = array('type' => $type, 'spendingLimit' => $_POST['spendingLimit'], 'userId' => $_SESSION['user']->userId);

		postToServer($data, $call);

    } else {

    	$choices = $_POST;

		$flatChoices = array();
		foreach ($choices as $choice) {

			if ($choice != 'null') {
				$flatChoices[] = (int) $choice;
			} else {
				$_SESSION['error'] = 'You must make a selection for each choice!';
				header("Location:{$home}/set-{$call}.php");
				exit;
			}
			
		}

		$cleanedChoices = array_unique($flatChoices);

		if ($call == 'music' && count($cleanedChoices) == 5 || $call == 'atmosphere' && count($cleanedChoices) == 3 || $call == 'venue' && count($cleanedChoices) == 3 ) {
			
			$data = array('type' => $type, $call => $cleanedChoices, 'userId' => $_SESSION['user']->userId);

			postToServer($data, $call);
			

		} else {

			$_SESSION['error'] = 'No duplicate choices allowed!';
			header("Location:{$home}/set-{$call}.php");
			exit;
			
		}

    }

	function getNextStep($call) {

		if ($call == 'music') {
			$output = 'atmosphere';
		} else if ($call == 'atmosphere') {
			$output = 'spendingLimit';
		} else if ($call == 'spendingLimit') {
			$output = 'venues';
		} else if ($call == 'info') {
			$output = 'music';
		} else if ($call == 'venue') {
			$output = 'done';
		}

		return $output;
			
	}

	function postToServer($data, $call) {

			$setDna = new httpPost('user', $data);
			$setDnaResponse = httpRequest::makePostRequest($setDna);

			$nextStep = getNextStep($call);

			if ($nextStep == 'done') {
				header("Location:login-handler.php?type=newUser");
			} else {
				header("Location:../set-{$nextStep}.php");
			}
			
	}
	
?>