
<?php

    if (!isset($_SESSION['venues'])) {

        // Building venue array

        $x = 0;
        $venues = array();

        while ($x < $venueCount) {

            $rand = rand(0,4);

            if ( $rand == 0) {
                $voted = 'up';
            } else if ($rand == 1) {
                $voted = 'down';
            } else {
                $voted = 'none';
            }

            $venues[$x] = new Venue;
            $venues[$x]->setName($venueNames[$x]);
            $venues[$x]->setDna(0, 0);
            $venues[$x]->setDistance(0, 0);

            $street = rand(1000,9999) . ' Reading Rd.';
            $venues[$x]->setStreet($street); 
            $venues[$x]->setCity('Cincinnati');
            $venues[$x]->setState('OH');
            $venues[$x]->setZip('45040');

            $phone = '513-' . rand(100,999) . '-' . rand(1000,9999);
            $venues[$x]->setPhone($phone);

            $url = 'www.' . str_replace('\'', '', str_replace(' ', '', strtolower($venueNames[$x]))) . '.com';
            $venues[$x]->setUrl($url);

            $music = $musicOpts[rand(0,8)];
            $atmosphere = $atmosphereOpts[rand(0,5)];
            $age = rand(18, 35);
            $spending = $spendingOpts[rand(0,3)];

            $venueDna = array(
                'music' => $music, 
                'atmosphere' => $atmosphere,
                'age' => $age,
                'spending' => $spending
            );
            
            $venues[$x]->setVenueDna($venueDna);

            $x ++;
        }

        $_SESSION['venues'] = $venues;

    }
?>