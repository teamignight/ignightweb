<?php 

	if (!isset($_SESSION['users'])) {

		$users = array();


		$userFnames = array(
			'Bill',
			'Tom',
			'George',
			'Rob',
			'Frank',
			'Henry',
			'Sam',
			'Lucas',
			'Eli',
			'Mike',
			'Don',
			'Marie',
			'Jessica',
			'Allison',
			'Abigail',
			'Sarah',
			'Michelle',
			'Erin',
			'Lara',
			'Judy',
			'Meredith',
			'Susan',
			'Ar-iaya',
			'Hamidou',
			'Abhinav',
			'Raimone'
		);

		$userLnames = array(
			'Chipman',
			'Simpson',
			'Smith',
			'De Lossi',
			'Abignail',
			'Thomas',
			'Franklin',
			'Rensing',
			'Haile',
			'Unger',
			'Chordia',
			'Soumare',
			'Brown'
		);

		rand(0,1) == 0 ? $gender = 'm' : $gender = 'f';

		$x = 0;
		while ($x < $userCount) {

			include('rand-dna-generator.php');

			$users[$x] = new User;
			$users[$x]->setFname($userFnames[rand(0,25)]);
			$users[$x]->setLname($userLnames[rand(0,12)]);
			$users[$x]->setDna($dna);
			$users[$x]->setCity('Cincinnati');
			$users[$x]->setState('OH');
			$users[$x]->setGender($gender);
			$users[$x]->setStatus('This is my status!');

			foreach ($_SESSION['venues'] as $venue) {
				$vote = rand(0,5);
				if ($vote == 2) {
					$users[$x]->setVenueVotes($users[$x]->addVenueVote($venue->getId(), 'up'));
					$venue->setVotes($venue->addVote($users[$x]->getId(), 'up'));
				} else if ($vote == 5) {
					$users[$x]->setVenueVotes($users[$x]->addVenueVote($venue->getId(), 'down'));
					$venue->setVotes($venue->addVote($users[$x]->getId(), 'down'));
				}
			}

			$x ++;

		}

		$_SESSION['users'] = $users; 

	}
?>

		