<?php 

    include('includes/header.php'); 

    if (isset($_POST)) {
    	$post = new httpPost('venue', $_POST);
		$response = httpRequest::makePostRequest($post);
    }


    if (isset($_GET['id'])) {
		$venue = getVenueDetails($user, $_GET['id']);
	}

    // Page variables

    $page = 'buzz';
    $pageTitle = $venue->getName();
    $section = 'buzz';

    // Load and calculate venues

    $venues = getTrendingList($user);

    include('includes/top-bar.php'); 

?>

<div id="main">
	<div class="chat">
		<div class="chat-comments">
		    <?php  

				$limit = 'all';
				include('includes/buzz/comment-list.php'); 

		    ?>
		</div>
		<div class="chat-users">
			<?php include('includes/buzz/comment-user-list.php'); ?>
		</div>
		<div class="chat-field">
			<form name="chat" method="post" action="buzz.php?id=<?php echo $venue->getId(); ?>">
				<div class="input-append">
					<input type="hidden" name="cityId" value="<?php echo $user->getCityId(); ?>" />
					<input type="hidden" name="venueId" value="<?php echo $venue->getId(); ?>" />
					<input type="hidden" name="userId" value="<?php echo $user->getId(); ?>" />
					<input type="hidden" name="type" value="addBuzz" />
	                <input name="buzzText" style="width: 249px" id="appendedInputButtons" placeholder="Say something interesting" type="text">
	                <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()"><i class="icon-comment"></i></button>
	            </div>
			</form>
		</div> 
	</div>
</div>

<?php include('includes/bottom-bar.php'); ?>

<?php include('includes/footer.php'); ?>



	