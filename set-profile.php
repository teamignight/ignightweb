<?php 

    $page = 'login';
    $pageTitle = 'Select Music';
    $section = 'setup';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    if (isset($_SESSION['error'])) {
    	$error = $_SESSION['error'];
    }

?>

<div id="main">

	<div class="steps clearfix">
		<ul>
			<li class="current"></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
		<div class="step-line"></div>
	</div>

	<?php 

		if (isset($error)) {
			echo '<p class="error">' . $error . '</p>';
		}
		unset($_SESSION['error']);

	?>

	<hr />

		
	<p>Please choose your average spending preferences.</p>

	<hr />

	<form class="login-form" action="functions/set-dna-handler.php?type=info" method="post">
		

		<div class="clearfix">
			<div class="pic-holder">
				<input type="hidden" name="picture" value="default.png" />
				Add Pic
			</div>
			<div class="name-holder">
				<input type="text" style="width: 205px;" name="firstName" placeholder="First Name" />
		        <input type="text" style="width: 205px;" name="lastName" placeholder="Last Name" />
		    </div>
		</div>               

		<input type="text" name="age" placeholder="Age" />      

        <select name="genderId">
            <option value="null">Choose Gender</option>
            <option value="0">Male</option>
            <option value="1">Female</option>
        </select>

        <hr />

        <?php
        
            echo '<select name="cityId"><option value="null">Choose City</option>';
    
            foreach ($_SESSION['setup']['cities'] as $k => $v) {
                echo '<option value="' . $k . '">' . $v . '</option>';
            }
    
            echo '</select>';
        ?>
 	

		
		<input type="submit" class="btn btn-success" value="Go To Next Step" />
	
	</form>

</div>

<?php include('includes/footer.php'); ?>
