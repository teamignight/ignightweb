<?php  

	require_once('../../classes/httpRequest.php');
	
	if ($_POST['method'] == 'get') {

		unset($_POST['method']);

		$type = $_POST['type'];
		unset($_POST['type']);

		$get = new httpGet('user', $type, $_POST);

		echo '<pre>';
		print_r($get);
		echo '</pre>';

		$response = json_decode(httpRequest::makeGetRequest($get));

	} else {
	
	if ($_POST['type'] == 'setDna') {

                        $everything = array();
                        $everything['type'] = $_POST['type'];
                        $everything['spendingLimit'] = $_POST['spendingLimit'];
                        $everything['userId'] = $_POST['userId'];

                        $flatMusic = array();
                        $flatAtmospheres = array();
                        $flatVenues = array();

                        foreach($_POST as $key => $value){
                                if(strpos($key, 'music') !== false) {
                                        $flatMusic[] = (int) $value;
                                }
                        }

                        foreach ($_POST as $key => $value) {
                                if(strpos($key, 'atmosphere') !== false) { 
                                        $flatAtmospheres[] = (int) $value;
                                }
                        }

                        foreach ($_POST as $key => $value) {
                                if(strpos($key, 'venue') !== false) { 
                                        $flatVenues[] = (int) $value;
                                }
                        }

                        $everything['music'] = $flatMusic;
                        $everything['atmosphere'] = $flatAtmospheres;
                        $everything['venue'] = $flatVenues;

                        unset($_POST);
                        $_POST = $everything;
                        $post = new httpPost('user', $_POST);
		$response = httpRequest::makePostRequest($post);

                } else{

		unset($_POST['method']);

		$post = new httpPost('user', $_POST);
		$response = httpRequest::makePostRequest($post);
		
		}
	}
	
	if ($response) {

		echo '<pre>';
		print_r($response);
		echo '</pre>';
		
	} else {

		echo '<p class="error">No response form server</p>';

	}

	echo '<br /><br /><a href="../index.php">Back to admin Panel</a>'

?>