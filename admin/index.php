<?php 

    include('includes/header.php'); 

    // Page variables

    $page = 'login';
    $pageTitle = 'IgNight Admin';
    $section = 'admin';

    include('../includes/top-bar.php'); 

?>

<div id="main">

    <div class="accordion" id="accordion">

	 <div class="accordion-group">

            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#group-api-get">
                    Group API - GET
                </a>
            </div>
            <div id="group-api-get" class="accordion-body collapse">
            <div class="accordion-inner">

					<form class="admin clearfix" method="post" action="handlers/user.php" name="getUserNameExists">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="trendingList" />

                        <label>Get Group Trending</label>


                            <input name="groupId" style="width: 253px" id="appendedInputButtons" placeholder="Enter groupID" type="text">
                            <input name="cityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter cityID" type="text">
                            <input name="userId" style="width: 253px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <input name="startIndex" style="width: 253px" id="appendedInputButtons" placeholder="Enter venue start index" type="text">
                            <input name="count" style="width: 253px" id="appendedInputButtons" placeholder="Enter number of venues" type="text">
                            
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                       

                    </form>
    </div>                
	</div>
		</div>	
	

        <div class="accordion-group">

            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#user-api-get">
                    User API - GET
                </a>
            </div>

            <div id="user-api-get" class="accordion-body collapse">
                <div class="accordion-inner">


					<form class="admin clearfix" method="post" action="handlers/user.php" name="testPushNotification">
                        
                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="testPushNotification" />

                        <label>Test Push Notification</label>

                        <div class="input-append">
                            <input name="userId" style="width: 210px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user.php" name="getUserNameExists">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="new" />

                        <label>Check if userName exists:</label>

                        <div class="input-append">
                            <input name="userName" style="width: 210px" id="appendedInputButtons" placeholder="Enter userName" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user.php" name="getUserInfo">
                        
                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getInfo" />

                        <label>Get User's info:</label>

                        <div class="input-append">
                            <input name="userId" style="width: 210px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user.php" name="getTrendingList">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="trendingList" />

                        <label>Get User's trending venues:</label>

                        <div class="input-append">
                            <input name="userId" style="width: 210px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user.php" name="getCheckInVenueHistory">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="venueHistory" />

                        <label>Get User's Check In history:</label>

                        <div class="input-append">
                            <input name="userId" style="width: 210px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>
                     <form class="admin clearfix" method="post" action="handlers/user.php" name="getIncomingGroupRequests">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getIncomingGroupRequests" />

                        <label>Get Incoming Group Requests</label>

                        <div class="input-append">
                            <input name="userId" style="width: 98px" id="appendedInputButtons" placeholder="userId" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user.php" name="getUserToUserDna">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="userToUserDna" />

                        <label>Get User to User Dna:</label>

                        <div class="input-append">
                            <input name="userOneId" style="width: 98px" id="appendedInputButtons" placeholder="userOneId" type="text">
                            <input name="userTwoId" style="width: 98px" id="appendedInputButtons" placeholder="userTwoId" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>
                    
                    <form class="admin clearfix" method="post" action="handlers/user.php" name="getUserGroups">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getUserGroups" />

                        <label>Get User Groups:</label>

                        <div class="input-append">
                            <input name="userId" style="width: 98px" id="appendedInputButtons" placeholder="userId" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>
                    
                    <form class="admin clearfix" method="post" action="handlers/group.php" name="getListOfUsersInGroup">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getListOfUsersInGroup" />

                        <label>Get List of Users in Groups:</label>

                        <div class="input-append">
                            <input name="groupId" style="width: 98px" id="appendedInputButtons" placeholder="groupId" type="text">
                            <input name="cityId" style="width: 98px" id="appendedInputButtons" placeholder="cityId" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>
                    

                </div>
            </div>
        
        </div>
        <div class="accordion-group">

            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#user-api-post">
                    User API - POST
                </a>
            </div>

            <div id="user-api-post" class="accordion-body collapse">
                <div class="accordion-inner">
					
					<form class="admin clearfix" method="post" action="handlers/user.php" name="addAndroidRegistrationId">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="addAndroidRegistrationId" />

                        <label>Add Android Registration Id</label>
                            <input name="userId" style="width: 253px" id="appendedInputButtons" placeholder="Enter userId" type="text">
                            <input name="androidRegistrationId" style="width: 253px" id="appendedInputButtons" placeholder="Enter androidRegistrationId" type="text">
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>
					
					
					<form class="admin clearfix" method="post" action="handlers/user.php" name="login">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="login" />

                        <label>Login</label>
                            <input name="userName" style="width: 253px" id="appendedInputButtons" placeholder="Enter userName" type="text">
                            <input name="password" style="width: 253px" id="appendedInputButtons" placeholder="Enter password" type="text">
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>
                    
					<form class="admin clearfix" method="post" action="handlers/user.php" name="updateCity">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="updateCity" />

                        <label>Update User City</label>
                            <input name="userId" style="width: 253px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <input name="newCityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter new city ID" type="text">
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>

					
					<form class="admin clearfix" method="post" action="handlers/group.php" name="groupTrendingList">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="addGroup" />

                        <label>Add New Group</label>

                            <input name="cityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter cityID" type="text">
                            <input name="userId" style="width: 253px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <input name="groupName" style="width: 253px" id="appendedInputButtons" placeholder="Enter groupName" type="text">
                            <input name="groupDescription" style="width: 253px" id="appendedInputButtons" placeholder="Enter groupName" type="text">
                           <input name="isPrivateGroup" style="width: 253px" id="appendedInputButtons" placeholder="isPrivateGroup" type="text">
                            
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                       

                    </form>
                    
                    <form class="admin clearfix" method="post" action="handlers/user.php" name="clearDailyUserData">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="clearDailyUserData" />

                        <label>Clear Daily User Data</label>
                            <input name="cityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter cityID" type="text">
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>
                    
                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="clearDailyVenueData">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="clearDailyVenueData" />

                        <label>Clear Daily Venue Data</label>
	                        <input name="cityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter cityID" type="text">
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>
                    
                    <form class="admin clearfix" method="post" action="handlers/group.php" name="inviteUser">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="inviteUser" />

                        <label>Invite User</label>
                            <input name="userId" style="width: 253px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <input name="groupId" style="width: 253px" id="appendedInputButtons" placeholder="Enter group ID" type="text">
                            <input name="cityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter city ID" type="text">
                            <input name="targetUserId" style="width: 253px" id="appendedInputButtons" placeholder="Enter target User Id" type="text">
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>
                    
                     <form class="admin clearfix" method="post" action="handlers/group.php" name="acceptInvite">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="acceptInvite" />

                        <label>Accept Invite</label>
                            <input name="userId" style="width: 253px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <input name="groupId" style="width: 253px" id="appendedInputButtons" placeholder="Enter group ID" type="text">
                            <input name="cityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter city ID" type="text">
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>


					<form class="admin clearfix" method="post" action="handlers/group.php" name="groupTrendingList">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="trendingList" />

                        <label>Get Group Trending</label>


                            <input name="groupId" style="width: 253px" id="appendedInputButtons" placeholder="Enter groupID" type="text">
                            <input name="cityId" style="width: 253px" id="appendedInputButtons" placeholder="Enter cityID" type="text">
                            <input name="userId" style="width: 253px" id="appendedInputButtons" placeholder="Enter userID" type="text">
                            <input name="startIndex" style="width: 253px" id="appendedInputButtons" placeholder="Enter venue start index" type="text">
                            <input name="count" style="width: 253px" id="appendedInputButtons" placeholder="Enter number of venues" type="text">
                            
                            <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                       

                    </form>
                    
                    
                    <form class="admin clearfix" method="post" action="handlers/user.php" name="addUser" >

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="add" />

                        <label>Add New User:</label>

                        <input type="text" style="width: 253px;" name="userName" placeholder="userName" />
                        <input type="text" style="width: 253px;" name="password" placeholder="password" />
                        <input type="text" style="width: 253px;" name="email" placeholder="email" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user_HS.php" name="updateProfilePicture" enctype="multipart/form-data">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="updateProfilePicture" />

                        <label>Update User Profile Picture:</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        User Image <INPUT type="file" name="userImage"><BR>
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user.php" name="setUserInfo">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="setUserInfo" />

                        <label>Add New User Info:</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <input type="text" style="width: 253px;" name="firstName" placeholder="firstName" />
                        <input type="text" style="width: 253px;" name="lastName" placeholder="lastName" />
                        <input type="text" style="width: 253px;" name="picture" placeholder="picture" />
                        
                        <select name="cityId">
                            <option value="null">Choose City</option>
                            <option value="0">Chicago</option>
                            <option value="1">Philadelphia</option>
                        </select>

                        <select name="genderId">
                            <option value="null">Choose Gender</option>
                            <option value="0">Male</option>
                            <option value="1">Female</option>
                        </select>

                        <input type="text" style="width: 253px;" name="age" placeholder="age" />

                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                    </form>

                    <form class="admin clearfix" method="post" action="handlers/user.php" name="setUserDna">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="setDna" />

                        <label>Add New User Dna:</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                            
                        <small>Music</small>
                            
                        <?php

                            $data = array();
                            $getMusicChoices = new httpGet('data', 'musicList', $data);
                            $getMusicChoicesResponse = json_decode(httpRequest::makeGetRequest($getMusicChoices));
                        
                            $musicChoices = json_decode($getMusicChoicesResponse->body);

                            $x = 1;
                            while($x < 6) {
                        
                                echo '<select name="music' . $x . '"><option value="null">Choose Music #' . $x . '</option>';
                        
                                foreach ($musicChoices as $musicChoice) {
                                    echo '<option value="' . $musicChoice->id . '">' . $musicChoice->name . '</option>';
                                }
                        
                                echo '</select>';
                                $x ++;

                            }
                        
                        ?>

                       <small>Atmosphere</small>
                        
                        <?php

                            $data = array();
                            $getAtmosphereChoices = new httpGet('data', 'atmosphereList', $data);
                            $getAtmosphereChoicesResponse = json_decode(httpRequest::makeGetRequest($getAtmosphereChoices));
                        
                            $atmosphereChoices = json_decode($getAtmosphereChoicesResponse->body);

                            $x = 1;
                            while($x < 4) {
                        
                                echo '<select name="atmosphere' . $x . '"><option value="null">Choose Atmosphere #' . $x . '</option>';
                        
                                foreach ($atmosphereChoices as $atmosphereChoice) {
                                    echo '<option value="' . $atmosphereChoice->id . '">' . $atmosphereChoice->name . '</option>';
                                }
                        
                                echo '</select>';
                                $x ++;

                            }
                        
                        ?>

                        <small>Spending</small>

                        <select name="spendingLimit">
                            <option value="null">Choose Spending Limit</option>
                            <option value="0">$</option>
                            <option value="1">$$</option>
                            <option value="2">$$$</option>
                            <option value="3">$$$$</option>
                        </select>  

                        <small>Venues</small><br />
                            
                        <input type="text" style="width: 253px;" name="venue1" placeholder="venueId #1" />
                        <input type="text" style="width: 253px;" name="venue2" placeholder="venueId #2" />
                        <input type="text" style="width: 253px;" name="venue3" placeholder="venueId #3" />

                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>



                </div>
            </div>
        
        </div>
        <div class="accordion-group">

            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#venue-api-get">
                    Venue API - GET
                </a>
            </div>

            <div id="venue-api-get" class="accordion-body collapse">
                <div class="accordion-inner">

                    <form class="admin clearfix" method="post" action="handlers/venue.php">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getInfo" />

                        <label>Get Venue Meta info:</label>
                        
                        <small style="font-weight: normal">Choose City to populate Venue options</small>
                        <div class="input-append">
                            <?php

                                $data = array();
                                $getCities = new httpGet('data', 'cities', $data);
                                $getCitiesResponse = json_decode(httpRequest::makeGetRequest($getCities));
                            
                                $cities = json_decode($getCitiesResponse->body);
                            
                                echo '<select id="venueInfo" name="cityId" onchange="javascript:loadVenues(1, venueInfo)"><option value="null">Choose City</option>';
                        
                                foreach ($cities as $city) {
                                    echo '<option value="' . $city->id . '">' . $city->name . '</option>';
                                }
                        
                                echo '</select>';
                            
                            ?>
                            <div class="venue-loader"></div>
                            <button class="btn" style="float: right;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>

                    </form>

                      <form class="admin clearfix" method="post" action="handlers/user.php" name="getUserTrendingList">
                        
                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="trendingList" />

                        <label>Get Trending List</label>

                            <input name="userId" style="width: 210px" id="appendedInputButtons" placeholder="Enter userId" type="text">
                            <input name="trendingVenueStartIndex" style="width: 210px" id="appendedInputButtons" placeholder="Enter venueStartIndex" type="text">
	                        <input name="trendingNoOfVenues" style="width: 210px" id="appendedInputButtons" placeholder="Enter no of venues" type="text">
	                        <input name="latitude" style="width: 210px" id="appendedInputButtons" placeholder="Enter latitude" type="text">
	                        <input name="longitude" style="width: 210px" id="appendedInputButtons" placeholder="Enter longitude" type="text">
	                        <input name="searchRadius" style="width: 210px" id="appendedInputButtons" placeholder="Enter searchRadius" type="text">
	                        <input name="trendingFilter" style="width: 210px" id="appendedInputButtons" placeholder="Enter trendingFilter" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
							                       

                    </form>

                      
                      <form class="admin clearfix" method="post" action="handlers/venue.php" name="getVenueDataForUser">
                        
                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getVenueDataForUser" />

                        <label>Get Data For User</label>

                            <input name="userId" style="width: 210px" id="appendedInputButtons" placeholder="Enter userId" type="text">
                            <input name="venueId" style="width: 210px" id="appendedInputButtons" placeholder="Enter venueId" type="text">
	                        <input name="noOfBuzzElements" style="width: 210px" id="appendedInputButtons" placeholder="Enter noOfBuzzElements" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
							                       

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="getVenueCrowdInfo">
                        
                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getCrowdStats" />

                        <label>Get Venue Crowd Info</label>

                      
                            <input name="cityId" style="width: 210px" id="appendedInputButtons" placeholder="Enter cityId" type="text">                        
                            <input name="venueId" style="width: 210px" id="appendedInputButtons" placeholder="Enter venueId" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                       

                    </form>

                       <form class="admin clearfix" method="post" action="handlers/venue.php" name="getAvailableConnections">
                        
                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="getAvailableConnections" />

                        <label>Get Venue Available Connections for User</label>

                      
                            <input name="userId" style="width: 210px" id="appendedInputButtons" placeholder="Enter UserId" type="text">                        
                            <input name="venueId" style="width: 210px" id="appendedInputButtons" placeholder="Enter venueId" type="text">
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                       

                    </form>

                    <!-- <form class="admin clearfix" method="post" action="handlers/venue.php">

                        <input type="hidden" name="method" value="get" />
                        <input type="hidden" name="type" value="new" />

                        <label>Check if Venue exists:</label>

                        <input name="venueName" style="width: 253px" placeholder="Enter venueName" type="text">
                        <input type="text" style="width: 253px;" name="lat" placeholder="latitude" />
                        <input type="text" style="width: 253px;" name="lon" placeholder="longitude" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>


                    </form> -->

                </div>
            </div>

        </div>

        <div class="accordion-group">

            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#venue-api-post">
                    Venue API - POST
                </a>
            </div>

            <div id="venue-api-post" class="accordion-body collapse">
                <div class="accordion-inner">

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="addVenue">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="add" />

                        <label>Add New Venue:</label>

                        <input type="text" style="width: 253px;" name="venueName" placeholder="venueName" />
                        <input type="text" style="width: 253px;" name="address" placeholder="address" />
                        <?php

                            $data = array();
                            $getCities = new httpGet('data', 'cities', $data);
                            $getCitiesResponse = json_decode(httpRequest::makeGetRequest($getCities));
                        
                            $cities = json_decode($getCitiesResponse->body);
                        
                            echo '<select name="cityId"><option value="null">Choose City</option>';
                            echo '<option value="null">Choose City</option>';
                            echo '<option value="0">Chicago</option>';
                            echo '<option value="1">New York</option>';

                            //foreach ($cities as $city) {
                            //    echo '<option value="' . $city->id . '">' . $city->name . '</option>';
                            //}
                    
                            echo '</select>';
                        
                        ?>
                        <input type="text" style="width: 253px;" name="zip" placeholder="zip" />
                        <input type="text" style="width: 253px;" name="state" placeholder="state" />
                        <input type="text" style="width: 253px;" name="number" placeholder="number" />
                        <input type="text" style="width: 253px;" name="url" placeholder="url" />
                        <input type="text" style="width: 253px;" name="lat" placeholder="latitude" />
                        <input type="text" style="width: 253px;" name="lon" placeholder="longitude" />

                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="upvoteVenue">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="upVote" />

                        <label>Up Vote Venue:</label>

                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="downvoteVenue">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="downVote" />

                        <label>Down Vote Venue:</label>

                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="checkInAtVenue">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="checkIn" />

                        <label>Check into Venue:</label>

                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="addVenueBuzz">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="addBuzz" />

                        <label>Add buzz text to Venue:</label>

                        <input type="text" style="width: 253px;" name="cityId" placeholder="cityId" />
                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <textarea name="buzzText" style="width:253px; height: 80px;" placeholder="buzzText"></textarea>
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue_HS.php" name="addVenueBuzz" enctype="multipart/form-data">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="addBuzz" />

                        <label>Add buzz image to Venue:</label>

                        <input type="text" style="width: 253px;" name="cityId" placeholder="cityId" />
                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        User Image <INPUT type="file" name="userImage"><BR>
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="blockAvailableConnectionUser">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="blockAvailableConnectionUser" />

                        <label>Block Connection User</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <input type="text" style="width: 253px;" name="userIdToBlock" placeholder="userIdToBlock" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="unBlockAvailableConnectionUser">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="unBlockAvailableConnectionUser" />

                        <label>Unblock Connection User</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <input type="text" style="width: 253px;" name="userIdToUnBlock" placeholder="userIdToUnBlock" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="setUserConnectionAvailabilityStatus">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="setUserConnectionAvailabilityStatus" />

                        <label>Set User Connection Availability</label>

                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                         <select name="userConnectionAvailabilityStatus">
                            <option value="null">Choose Status</option>
                            <option value="True">Available to connect</option>
                            <option value="False">Not Available to conect</option>
                        </select>
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                     <form class="admin clearfix" method="post" action="handlers/venue.php" name="sendConnectRequest">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="sendConnectRequest" />

                        <label>Send Connect Request:</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <input type="text" style="width: 253px;" name="connectTargetUserId" placeholder="connectTargetUserId" />
                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="acceptConnectRequest">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="acceptConnectRequest" />

                        <label>Accept Connect Request:</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <input type="text" style="width: 253px;" name="connectRequestorUserId" placeholder="connectRequestorUserId" />
                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                     <form class="admin clearfix" method="post" action="handlers/venue.php" name="denyConnectRequest">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="denyConnectRequest" />

                        <label>Deny Connect Request:</label>

                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <input type="text" style="width: 253px;" name="connectRequestorUserId" placeholder="connectRequestorUserId" />
                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>

                    <form class="admin clearfix" method="post" action="handlers/venue.php" name="sendConnectMessage">

                        <input type="hidden" name="method" value="post" />
                        <input type="hidden" name="type" value="sendConnectMessage" />

                        <label>Send Connect Message:</label>

                        <input type="text" style="width: 253px;" name="venueId" placeholder="venueId" />
                        <input type="text" style="width: 253px;" name="userId" placeholder="userId" />
                        <input type="text" style="width: 253px;" name="connectionId" placeholder="connectionId" />
                        <textarea name="connectMessageText" style="width:253px; height: 80px;" placeholder="connectMessageText"></textarea>
                        <button class="btn" style="float: right; margin-bottom: 15px;" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>

                    </form>
                    
                </div>
            </div>

        </div>

        <div class="accordion-group">

            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#data-api">
                    Data API - GET
                </a>
            </div>

            <div id="data-api" class="accordion-body collapse">
                <div class="accordion-inner">
                    
                    <a class="btn btn-warning full" href="handlers/data.php?type=cities">Get cities</a>
                    <a class="btn btn-warning full" href="handlers/data.php?type=musicList">Get musicList</a>
                    <a class="btn btn-warning full" href="handlers/data.php?type=atmosphereList">Get atmosphereList</a>
                    <a class="btn btn-warning full" href="handlers/data.php?type=spendingLimitRange">Get spendingLimitRange</a>
                        

                    <form class="admin clearfix" method="post" action="handlers/data.php">
                        <label>Get Venues For City Id:</label>
                        <div class="input-append">
                            <input name="cityId" style="width: 210px" id="appendedInputButtons" placeholder="Enter cityId" type="text">
                            <input type="hidden" name="type" value="venueList" />
                            <button class="btn" type="button" onclick="javascript:$(this).closest(form).submit()">Go</button>
                        </div>
                    </form>

                </div>
            </div>

        </div>

    </div>
    
</div>

<script>
    
    function loadVenues(count, id) {
        var url = 'includes/venues.php?count=' + count + '&id=' + $(id).val();
        $(id).siblings('.venue-loader').load(url);
    }

</script>

<?php include('includes/footer.php'); ?>