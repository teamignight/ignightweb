<?php

	class User {

		// Class properties 

		static $index = 0;

		// Instance properties

		public $id;
		public $userName;
		public $fname;
		public $lname;
		public $dna;
		public $cityId;
		public $state;
		public $gender;
		public $status;
		public $comments = array();
		public $venueVotes = array();
		public $commentVotes = array();

		// Constructor

		public function __construct() {
	        $this->dna = new Dna;
	        $this->id = self::$index;
	        self::$index ++;    
	    }

		// Setters & Getters

		public function getId() {
			return $this->id;
		}
		public function setId($id) {
			$this->id = $id;
		}

		public function getUserName() {
			return $this->userName;
		}
		public function setUserName($userName) {
			$this->userName = $userName;
		}

		public function getFname() {
			return $this->fname;
		}
		public function setFname($fname) {
			$this->fname = $fname;
		}

		public function getLname() {
			return $this->lname;
		}
		public function setLname($lname) {
			$this->lname = $lname;
		}

		public function getDna() {
			return $this->dna;
		}
		public function setDna($dna) {
			$this->dna->setTopMusic($dna->getTopMusic());
			$this->dna->setTopAtmospheres($dna->getTopAtmospheres());
			$this->dna->setTopVenues($dna->getTopVenues());
			$this->dna->setAge($dna->getAge());
			$this->dna->setSpending($dna->getSpending());
		}

		public function getCityId() {
			return $this->cityId;
		}
		public function setCityId($cityId) {
			$this->cityId = $cityId;
		}

		public function getState() {
			return $this->state;
		}
		public function setState($state) {
			$this->state = $state;
		}

		public function getGender() {
			return $this->gender;
		}
		public function setGender($gender) {
			$this->gender = $gender;
		}

		public function getStatus() {
			return $this->status;
		}
		public function setStatus($status) {
			$this->status = $status;
		}

		public function getComments() {
			return $this->comments;
		}
		public function setComments($comments) {
			$this->comments = $comments;
		}

		public function getVenueVotes() {
			return $this->venueVotes;
		}
		public function setVenueVotes($upVotes, $downVotes) {
			$this->venueVotes = array('upVotes' => $upVotes, 'downVotes' => $downVotes);
		}

		public function getCommentVotes() {
			return $this->commentVotes;
		}
		public function setCommentVotes($commentVotes) {
			$this->commentVotes = $commentVotes;
		}

		// Instance methods

		public function addComment($comment) {
			$this->comments[] = $comment;
		}

		public function addCommentVote($commentId) {
			$commentVotes = $this->getCommentVotes();
			$commentVotes[] = $commentId;
			return $commentVotes;
		}

		public function addVenueVote($venueId, $vote) {
			$venueVotes = $this->getVenueVotes();
			$venueVotes[] = array('venueId' => $venueId, 'vote' => $vote);
			return $venueVotes;
		}

		public function calcDna($user1, $user2) {
			return rand(0,100);
		}

		public function getUserValueClass($dna) {

			if ($dna > 80) {
		 		$dnaClass = 'high';
		 	} else if ($dna > 60) {
		 		$dnaClass = 'med-high';
		 	} else if ($dna > 40) {
		 		$dnaClass = 'med';
		 	} else if ($dna > 10) {
		 		$dnaClass = 'low';
		 	} else {
		 		$dnaClass = '';
		 	}

		 	return $dnaClass;
		}

		static function initFromJson($json) {

			$user = new User;
			$dna = new Dna;

			$user->setId($json->userId);
			$user->setUserName($json->userName);
			$user->setFname($json->firstName);
			$user->setLname($json->lastName);
			$user->setGender($json->genderId);
			$user->setCityId($json->cityId);

			$dna->setAge($json->age);
			$dna->setTopAtmospheres($json->dna->atmospherePrefs);
			$dna->setTopMusic($json->dna->musicPrefs);
			$dna->setTopVenues($json->dna->venuePrefs);
			$dna->setSpending($json->dna->spendingLimit);

			$user->setDna($dna);

			$data = array('userId' => $user->getId());
			$getUpVotes = new httpGet('user', 'upVotes', $data);
			$upVotes = json_decode(httpRequest::makeGetRequest($getUpVotes));

			$upVotes = $upVotes->body;

			$data = array('userId' => $user->getId());
			$getDownVotes = new httpGet('user', 'downVotes', $data);
			$downVotes = json_decode(httpRequest::makeGetRequest($getDownVotes));

			$downVotes = $downVotes->body;

			$user->setVenueVotes($upVotes, $downVotes);


			return $user;

		}

		static function initWithId($id) {

			$data = array('userId' => $id);
			$getInfo = new httpGet('user', 'getInfo', $data);
			$response = json_decode(httpRequest::makeGetRequest($getInfo));

			$user = User::initFromJson($response->body);

			return $user;

		}


	}

?>
