(function() {
	$('.tt').tooltip();
	if ($(".chat-comments").length > 0) {
		$(".chat-comments").scrollTop($(".chat-comments")[0].scrollHeight);
	}
})();

$(document).on('click', '.doVote', function (e){
	var type;
	var id = $(this).data('id');
	if ($(this).hasClass('upVote')) {
		type = 'upVote';
	} else if ($(this).hasClass('downVote')) {
		type = 'downVote';
	}
	$(this).closest('.vote').load('includes/trending/vote.php?id=' + id + '&type=' + type + '&vote=true');

});
