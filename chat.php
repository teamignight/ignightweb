<?php 
    
    $page = 'chat';
    $pageTitle = 'Chat';
    $section = 'buzz';

    include('includes/header.php'); 
    include('includes/top-bar.php'); 

    $user = $_SESSION['users'][$_GET['id']];
    $id = $user->getId();
    $name = $user->getFname() . ' ' . $user->getLname();
    $location = $user->getCity() . ', ' . $user->getState();
    $status = $user->getStatus();

    $dna = $user->dna;
    $music = $dna->getTopMusic();
    $venues = $dna->getTopVenues();
    $atmospheres = $dna->getTopAtmospheres();
    $age = $dna->age;
    $spending = $dna->spending;


?>

<div id="main">

	<div class="user-card clearfix">
		<div class="user-pic">
			<img src="#" alt="<?php echo $name; ?>">
		</div>
		<div class="user-info">
			<div class="user-name"><?php echo $name; ?></div>
			<div class="user-loc"><?php echo $location; ?></div>
			<div class="user-status">
				<?php echo $status; ?>
			</div>
		</div>
	</div>

	<div class="recent-activity">
		<hr />
		<div class="subtitle">
			<h5><i class="icon icon-comment"></i>Your Conversation</h5>
		</div>
		<ul class="buzz">
			<?php 

				foreach ($user->comments as $comment) {
					foreach($_SESSION['venues'] as $venue) {
						if ($comment->getVenueId() == $venue->getId()) {
							$commentVenue = $venue;
						} 
					}

					$commentVenueName = $commentVenue->getName();
					$commentContent = $comment->getContent();
					$commentVotes = $comment->getVotes();

			?>

				<li>
					<div class="clearfix">
						<a href="venue.php?id=<?php echo $commentVenue->getId(); ?>" class="user high tt" data-toggle="tooltip" title="<?php echo $commentVenueName; ?>">
							<i class="icon-white icon-map-marker"></i>
						</a>
						<div class="content">
							<?php echo $commentContent; ?>
						</div>
					</div>
					<div class="votes">
						<?php 
							if ($commentVotes > 0) { echo 'This comment has ' . $commentVotes . ' votes.'; } 
						?>
					</div>
					
				</li>

			<?php } ?>

		</ul>

	</div>



    
</div>

<?php include('includes/bottom-bar.php'); ?>

<?php include('includes/footer.php'); ?>